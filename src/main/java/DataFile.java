import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;

/**
 * File: DataFile.java
 * Project: Image Classifier
 * Author: Kathy Applebaum
 * Created: Sep 19, 2015
 * <p/>
 * Creates a datafile and an answer file for a given number of images for a given question
 */
public class DataFile {
    static final String TRAINING_EXT = "train.csv";
    static final String TEST_EXT = "test.csv";

    static final String IMAGE_SINGLE_STR = "_img_";
    static final String CLASS_SINGLE_STR = "_class_";
    static final String PETRO_SINGLE_STR = "_petro_";

    static final String IMAGE_BAGGING_STR = "_img_bag_";
    static final String CLASS_BAGGING_STR = "_class_bag_";
    static final String PETRO_BAGGING_STR = "_petro_bag_";

    int totalIds;
    int trainingIdCount;
    String fileBaseName;
    ZooQuestion zooQuestion;
    boolean needsRotate;

    public DataFile(int totalIds, int trainingIdCount, String fileBaseName, ZooQuestion zooQuestion) {
        this.totalIds = totalIds;
        this.trainingIdCount = trainingIdCount;
        this.fileBaseName = fileBaseName;
        this.zooQuestion = zooQuestion;
    }

    public void setFileBaseName(String fileBaseName) {
        this.fileBaseName = fileBaseName;
    }

    public void buildSingleSVMfiles(float agreementPercent, ExecutorService executor) {
        // Get a list of ids appropriate for this question. If there aren't enough, duplicate ids until we have enough.
        // We'll randomly flip and rotate later, so duplicates likely won't be the same as originals.
        List<String> idList = DatabaseUtils.getIdListForQuestion(zooQuestion, totalIds, agreementPercent);
        int additional = totalIds - idList.size();
        needsRotate = additional > 0;
        if (needsRotate) {
            idList = padList(idList, additional);
        }

        // Spin off threads to write files
        final int testingIdCount = totalIds - trainingIdCount;
        final List<String> testingIds = idList.subList(0, testingIdCount);
        final List<String> trainingIds = idList.subList(testingIdCount, totalIds);

        ImageWriterTask imageTrainingTask = new ImageWriterTask(trainingIds, fileBaseName + IMAGE_SINGLE_STR + TRAINING_EXT, needsRotate);
        executor.submit(imageTrainingTask);

        ImageWriterTask imageTestingTask = new ImageWriterTask(testingIds, fileBaseName + IMAGE_SINGLE_STR + TEST_EXT, needsRotate);
        executor.submit(imageTestingTask);

        ClassWriterTask classTrainingTask = new ClassWriterTask(trainingIds, fileBaseName + CLASS_SINGLE_STR + TRAINING_EXT);
        executor.submit(classTrainingTask);

        ClassWriterTask classTestingTask = new ClassWriterTask(testingIds, fileBaseName + CLASS_SINGLE_STR + TEST_EXT);
        executor.submit(classTestingTask);

        PetroWriterTask petroTrainingTask = new PetroWriterTask(trainingIds, fileBaseName + PETRO_SINGLE_STR + TRAINING_EXT);
        executor.submit(petroTrainingTask);

        PetroWriterTask petroTestingTask = new PetroWriterTask(testingIds, fileBaseName + PETRO_SINGLE_STR + TEST_EXT);
        executor.submit(petroTestingTask);
    }


    public void buildBaggingFiles(float agreementPercent, int bagCount, ExecutorService executor) {
        List<String> idList = DatabaseUtils.getIdListForQuestion(zooQuestion, totalIds, agreementPercent);
        int additional = totalIds - idList.size();
        needsRotate = additional > 0;
        if (needsRotate) {
            idList = padList(idList, additional);
        }

        // Spin off threads to write files.
        final int testingIdCount = totalIds - trainingIdCount;
        final List<String> testingIds = idList.subList(0, testingIdCount);
        final List<String> trainingIds = idList.subList(testingIdCount, totalIds);

        // Spin off the testing threads now
        ImageWriterTask imageTestingTask = new ImageWriterTask(testingIds, fileBaseName + IMAGE_SINGLE_STR + TEST_EXT, needsRotate);
        executor.submit(imageTestingTask);

        ClassWriterTask classTestingTask = new ClassWriterTask(testingIds, fileBaseName + CLASS_SINGLE_STR + TEST_EXT);
        executor.submit(classTestingTask);

        PetroWriterTask petroTestingTask = new PetroWriterTask(testingIds, fileBaseName + PETRO_SINGLE_STR + TEST_EXT);
        executor.submit(petroTestingTask);

        Random random = new Random();
        for (int i = 0; i < bagCount; i++) {
            final List<String> baggingIds = new ArrayList<>(trainingIdCount);
            for (int j = 0; j < trainingIdCount; j++) {
                baggingIds.add(trainingIds.get(random.nextInt(trainingIdCount)));
            }
            ImageWriterTask imageTrainingTask = new ImageWriterTask(baggingIds, fileBaseName + IMAGE_BAGGING_STR + (i+1) + TRAINING_EXT, needsRotate);
            executor.submit(imageTrainingTask);
            ClassWriterTask classTrainingTask = new ClassWriterTask(baggingIds, fileBaseName + CLASS_BAGGING_STR + (i+1) + TRAINING_EXT);
            executor.submit(classTrainingTask);
            PetroWriterTask petroTrainingTask = new PetroWriterTask(baggingIds, fileBaseName + PETRO_BAGGING_STR + (i+1) + TRAINING_EXT);
            executor.submit(petroTrainingTask);
        }
    }

    private List<String> padList(List<String> originalList, final int additional) {
        Random random = new Random();
        final int listSize = originalList.size();
        List<String> paddedList = new ArrayList<>(listSize + additional);
        paddedList.addAll(originalList);
        for (int i = 1; i <= additional; i++) {
            int index = random.nextInt(listSize);
            paddedList.add(originalList.get(index));
        }
        return paddedList;
    }

    private class PetroWriterTask implements Runnable {
        final List<String> idList;
        final String fileName;

        public PetroWriterTask(List<String> idList, String fileName) {
            this.idList = idList;
            this.fileName = fileName;
        }

        @Override
        public void run() {
            createPetroFile(idList, fileName);
        }

        private void createPetroFile(List<String> idList, String fileName) {
            File file = new File(fileName);
            try {
                file.createNewFile();
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                    for (String id : idList) {
                        String petroData = DatabaseUtils.getPetroForId(id);
                        writer.write(petroData + "\n");
                    }
                    System.out.println(String.format("File %s completed", fileName));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class ClassWriterTask implements Runnable {
        final List<String> idList;
        final String fileName;

        public ClassWriterTask(List<String> idList, String fileName) {
            this.idList = idList;
            this.fileName = fileName;
        }

        @Override
        public void run() {
            createClassFile(idList, fileName);
        }

        private void createClassFile(List<String> idList, String fileName) {
            File file = new File(fileName);
            try {
                file.createNewFile();
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                    for (String id : idList) {
                        String classData = DatabaseUtils.getAnswersForId(id);
                        writer.write(classData + "\n");
                    }
                    System.out.println(String.format("File %s completed", fileName));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class ImageWriterTask implements Runnable {
        final List<String> idList;
        final String fileName;
        final boolean needsRotate;

        public ImageWriterTask(List<String> idList, String fileName, boolean needsRotate) {
            this.idList = idList;
            this.fileName = fileName;
            this.needsRotate = needsRotate;
        }

        @Override
        public void run() {
            createImageFile(idList, fileName, needsRotate);
        }

        private void createImageFile(List<String> idList, String fileName, boolean needsRotate) {
            Random random = new Random();
            File file = new File(fileName);
            try {
                file.createNewFile();
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                    for (String id : idList) {
                        GalaxyImage galaxyImage = new GalaxyImage(id);
                        if (needsRotate) {
                            int rotate = random.nextInt(4);
                            switch (rotate) {
                                case 1:
                                    galaxyImage.rotate90();
                                    break;
                                case 2:
                                    galaxyImage.rotate180();
                                    break;
                                case 3:
                                    galaxyImage.rotate270();
                                    break;
                                case 0:
                                default:
                                    ;   // no-op
                            }
                            if (random.nextBoolean()) {
                                galaxyImage.flipHorizontal();
                            }
                            if (random.nextBoolean()) {
                                galaxyImage.flipVertical();
                            }
                        }
                        writer.write(galaxyImage.toString() + "\n");
                    }
                    System.out.println(String.format("File %s completed", fileName));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
