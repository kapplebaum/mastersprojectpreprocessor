import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import net.miginfocom.swing.MigLayout;

/**
 * File: ImagePanel
 * Project: Image Classifier
 * Author: Kathy Applebaum
 * Created: Sep 05, 2015
 * <p/>
 * <p/>
 * Reads image bytes from a file and displays in a JPanel
 */
public class ImagePanel extends JPanel {
	static final int DEFAULT_COLUMNS = 5;
	static final int IMAGE_WIDTH = 140;
	static final int IMAGE_HEIGHT = 140;


	String fileName;
	int columns;

	public ImagePanel(String fileName) {
		super(new MigLayout("wrap " + DEFAULT_COLUMNS));
		this.fileName = fileName;
		columns = DEFAULT_COLUMNS;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}


	// todo To work, image needs to be 140x140. This should be improved.
	public void display() {
		display(Integer.MAX_VALUE);
	}

	// todo To work, image needs to be 140x140. This should be improved.
	public void display(int numberOfImages) {
		BufferedReader bufferedReader;
		String currentLine;
		int counter = 0;
		try {
			bufferedReader = new BufferedReader(new FileReader(fileName));
			while (counter < numberOfImages && (currentLine = bufferedReader.readLine()) != null) {
				String[] pixelValues = currentLine.split(",");
				byte[] pixelBytes = new byte[pixelValues.length - 1];    // we'll toss the ID
				for (int i = 1; i < pixelValues.length; i++) {
					pixelBytes[i - 1] = Byte.parseByte(pixelValues[i].trim());
				}
				BufferedImage image = getImageFromBytes(pixelBytes);
				this.add(new JLabel(new ImageIcon(image)));
				counter++;
			}
		} catch (FileNotFoundException e) {
			// todo better error handling
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private BufferedImage getImageFromBytes(byte[] bytes) {
		BufferedImage bufferedImage = new BufferedImage(140, 140, BufferedImage.TYPE_BYTE_GRAY);
		int counter = 0;
		for (int i = 0; i < IMAGE_WIDTH; i++) {
			for (int j = 0; j < IMAGE_HEIGHT; j++) {
				int grayValue = bytes[(i * IMAGE_WIDTH) + j];
				int colorValue = (((grayValue << 8) + grayValue) << 8) + grayValue;
				bufferedImage.setRGB(i, j, colorValue);
			}
		}
		return bufferedImage;
	}
}
