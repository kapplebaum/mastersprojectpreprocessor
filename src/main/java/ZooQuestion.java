/**
 * File: ${FILE_NAME}
 * Project: Image Classifier
 * Author: Kathy Applebaum
 * Created: Sep 19, 2015
 * <p/>
 */
public enum ZooQuestion {
	All, Question01, Question02, Question03, Question04, Question05, Question06, Question07, Question08, Question09, Question10, Question11;

	@Override
	public String toString() {
		switch (this) {
			case All:
				return "All";
			case Question01:
				return "Question 1";
			case Question02:
				return "Question 2";
			case Question03:
				return "Question 3";
			case Question04:
				return "Question 4";
			case Question05:
				return "Question 5";
			case Question06:
				return "Question 6";
			case Question07:
				return "Question 7";
			case Question08:
				return "Question 8";
			case Question09:
				return "Question 9";
			case Question10:
				return "Question 10";
			case Question11:
				return "Question 11";
		}
		return "";
	}
}
