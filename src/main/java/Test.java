import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Project: Image Classifier
 * Author: Kathy Applebaum
 * Created: Aug 29, 2015
 * <p/>
 */
public class Test {
    public static void main(String[] args) {
        final int THREADS = 3;    // tasks are IO bound, so 2-3 threads is about as efficient as we'll get
        ExecutorService executor = Executors.newFixedThreadPool(THREADS);

        final float agreementPercent = 0.80f;
        int limit = 14000;
        int trainingCount = 10000;
        for (ZooQuestion question : ZooQuestion.values()) {
            // Question 1 is the same as all. No reason to duplicate
            if (question != ZooQuestion.All) {
                System.out.println(String.format("Working on %s", question.toString()));
                DataFile dataFile = new DataFile(limit, trainingCount, "data/single/" + question.name(), question);

                System.out.println("Building single SVM files");
                dataFile.buildSingleSVMfiles(agreementPercent, executor);

                System.out.println("Building bagging SVM files");
                dataFile.setFileBaseName("data/bagging/" + question.name());
                dataFile.buildBaggingFiles(agreementPercent, 3, executor);

            }
        }
        executor.shutdown();
    }
}
