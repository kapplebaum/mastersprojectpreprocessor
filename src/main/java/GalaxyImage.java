import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.text.DecimalFormat;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.w3c.dom.Document;

/**
 * File: GalaxyImage.java
 * Project: Image Classifier
 * Author: Kathy Applebaum
 * Created: Aug 29, 2015
 * <p/>
 * Handles retrieving a single image from the database, downloading from the SDSS Sky Server if needed, and manipulating it for saving
 */
public class GalaxyImage {
    public static final int IMAGE_SIZE = 140;
    private static final String URL_STRING = "http://skyserver.sdss.org/ImgCutoutDR7/ImgCutout.asmx/GetJpeg";
    private static final String CHARSET = "UTF-8";
    private static final String DRAWING_OPT = "opt_=\"\"";
    private static final String ELEMENT_NAME = "base64Binary";
    private static final DecimalFormat myFormatter = new DecimalFormat("0.####");


    byte[] image = null;
    String id;

    public GalaxyImage(@Nonnull String id) {
        this.id = id;
        this.image = getImageAsBytes();
    }

    public GalaxyImage(GalaxyImage galaxyImage) {
        this.id = galaxyImage.getId();
        this.image = new byte[galaxyImage.image.length];
        System.arraycopy(galaxyImage.image, 0, this.image, 0, galaxyImage.image.length);
    }

    public byte[] getImageAsBytes() {
        if (image == null) {
            // look in database
            image = getImageFromDatabase();

            if (image == null) {
                // Wasn't in database, so retrieve it, save in database, and return
                image = getImageFromSkyserver();
                saveImageToDatabase();
            }
        }
        assert image != null;
        return image;
    }

    public String getId() {
        return id;
    }

    @Nullable
    private byte[] getImageFromDatabase() {
        return DatabaseUtils.fetchGalaxyImage(id);
    }

    @Nullable
    private byte[] getImageFromSkyserver() {
        GalaxyMetadata metadata = DatabaseUtils.getMetadata(id);

        final String raString = "ra_=" + metadata.getRa();
        final String decString = "dec_=" + metadata.getDec();
        final String scaleString = "scale_=" + metadata.getScale();
        final String widthString = "width_=" + metadata.getWidth();
        final String heightString = "height_=" + metadata.getHeight();

        try {
            String httpQuery = URL_STRING + "?" + raString + "&" + decString + "&" + scaleString + "&"
                    + widthString + "&" + heightString + "&" + DRAWING_OPT;
            URLConnection connection = new URL(httpQuery).openConnection();
            connection.setRequestProperty("Accept-Charset", CHARSET);
            InputStream response = connection.getInputStream();
            DocumentBuilderFactory xmlFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = xmlFactory.newDocumentBuilder();
            Document xmlDoc = docBuilder.parse(response);
            XPathFactory xpathFact = XPathFactory.newInstance();
            XPath xpath = xpathFact.newXPath();
            String b64Image = (String) xpath.evaluate(ELEMENT_NAME, xmlDoc,
                    XPathConstants.STRING);
            byte[] colorJPG = Base64.decode(b64Image);
            return convertToGrayscale(colorJPG);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void saveImageToDatabase() {
        assert image != null;

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    DatabaseUtils.saveGalaxyImage(id, image);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public BufferedImage getImageAsBufferedImage() {
        if (image == null) {
            getImageAsBytes();
        }
        assert image != null;
        int[] tempImage = new int[image.length];
        int counter = 0;
        for (byte pixel : image) {
            tempImage[counter] = (int) pixel;
            counter++;
        }

        BufferedImage bufferedImage = new BufferedImage(IMAGE_SIZE, IMAGE_SIZE, BufferedImage.TYPE_BYTE_GRAY);
        bufferedImage.getRaster().setPixels(0, 0, IMAGE_SIZE, IMAGE_SIZE, tempImage);
        return bufferedImage;
    }

    private byte[] convertToGrayscale(byte[] colorImage) throws IOException {

        BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(colorImage));
        BufferedImage grayscaleImage = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        ColorConvertOp convertOp = new ColorConvertOp(bufferedImage.getColorModel().getColorSpace(), grayscaleImage.getColorModel().getColorSpace(), null);
        convertOp.filter(bufferedImage, grayscaleImage);
        int index = 0;
        int width = grayscaleImage.getWidth();
        int height = grayscaleImage.getHeight();
        byte[] grayImage = new byte[width * height];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                Color c = new Color(grayscaleImage.getRGB(i, j));
                grayImage[index] = (byte) c.getRed();    // red == blue == green
                index++;
            }
        }
        return grayImage;
    }

    public void flipHorizontal() {
        for (int index = 0; index < image.length; index++) {
            int column = index % IMAGE_SIZE;
            if (column < IMAGE_SIZE / 2) {
                int row = index / IMAGE_SIZE;
                byte temp = image[index];
                image[index] = image[(row * IMAGE_SIZE) + (IMAGE_SIZE - 1 - column)];
                image[(row * IMAGE_SIZE) + (IMAGE_SIZE - 1 - column)] = temp;
            }
        }
    }


    public void flipVertical() {
        for (int index = 0; index < image.length / 2; index++) {
            int column = index % IMAGE_SIZE;
            int row = index / IMAGE_SIZE;

            byte temp = image[index];
            image[index] = image[((IMAGE_SIZE - 1-row) * IMAGE_SIZE) + column];
            image[((IMAGE_SIZE - 1-row) * IMAGE_SIZE) + column] = temp;

        }
    }

    public void rotate90() {
        byte[] temp = new byte[image.length];
        for (int index = 0; index < image.length; index++) {
            int column = index % IMAGE_SIZE;
            int row = index / IMAGE_SIZE;
            temp[((column) * IMAGE_SIZE) + (IMAGE_SIZE - 1 - row)] = image[index];
        }
        image = temp;
    }

    public void rotate180(){
        byte[] temp = new byte[image.length];
        for (int index = 0; index < image.length; index++) {
            int column = index % IMAGE_SIZE;
            int row = index / IMAGE_SIZE;
            temp[((IMAGE_SIZE - 1-row) * IMAGE_SIZE) + (IMAGE_SIZE - 1 - column)] = image[index];
        }
        image = temp;
    }

    public void rotate270(){
        byte[] temp = new byte[image.length];
        for (int index = 0; index < image.length; index++) {
            int column = index % IMAGE_SIZE;
            int row = index / IMAGE_SIZE;
            temp[((IMAGE_SIZE - 1-column) * IMAGE_SIZE) + row] = image[index];
        }
        image = temp;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(adjustByte(image[0]));
        for (int i = 1; i < image.length; i++) {
            sb.append(",").append(adjustByte(image[i]));
        }
        return sb.toString();
    }


    /**
     * The original value range was 0-255. When storing as bytes, values >= 128 become negative numbers due to
     * overflow. We want to convert any negative numbers back to the correct positive numbers.
     *
     * @param pixel
     * @return
     */
    private String adjustByte(byte pixel) {
        int rbgvalue = pixel >= 0 ? pixel : (int) (pixel + 256);
        double pixelValue = rbgvalue / 255.0;
        return myFormatter.format(pixelValue);
    }

}
