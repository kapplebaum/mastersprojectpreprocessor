import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

/**
 * File: DatabaseUtils.java
 * Project: Image Classifier
 * Author: Kathy Applebaum
 * Created: Aug 29, 2015
 * <p/>
 */
public class DatabaseUtils {
    static String username;
    static String password;
    static String url;
    static String driver;

    /**
     * Load the properties file.
     */
    private static void loadProperties() {
        Properties prop = new Properties();
        try (InputStream in = DatabaseUtils.class.getResourceAsStream("database.properties")){
            prop.load(in);
            username = prop.getProperty("username");
            password = prop.getProperty("password");
            url = prop.getProperty("url");
            driver = prop.getProperty("driver");
        } catch (IOException e) {
            System.out.println("Could not load database parameters, using default values.");
            System.out.println("Check values in database.properties.");
        }
    }

    /**
     * Saves a GalaxyImage object to the database
     *
     * @throws SQLException
     */
    public static void saveGalaxyImage(@Nonnull String id, @Nonnull byte[] image) throws SQLException {
        if (driver == null) {
            loadProperties();
        }
        DbUtils.loadDriver(driver);
        DriverManager.setLoginTimeout(5);
        Connection conn = DriverManager.getConnection(url, username, password);
        PreparedStatement stmt = null;
        String insertString = "INSERT INTO astro.images (dr7objid, image) VALUES (?,?)";
        try {
            conn.setAutoCommit(false);
            stmt = conn.prepareStatement(insertString);
            stmt.setString(1, id);
            stmt.setBytes(2, image);
            stmt.executeUpdate();
            conn.commit();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
            conn.setAutoCommit(true);
            conn.close();
        }
    }

    @Nullable
    public static byte[] fetchGalaxyImage(@Nonnull String id) {
        Connection conn = null;
        byte[] image = null;
        if (driver == null) {
            loadProperties();
        }
        DbUtils.loadDriver(driver);
        DriverManager.setLoginTimeout(5);
        try {
            conn = DriverManager.getConnection(url, username, password);
            ResultSetHandler<byte[]> resultSetHandler = new ResultSetHandler<byte[]>() {
                @Override
                public byte[] handle(ResultSet resultSet) throws SQLException {
                    byte[] result = null;
                    if (resultSet.next()) {
                        result = resultSet.getBytes("image");
                    }
                    return result;
                }
            };
            QueryRunner query = new QueryRunner();
            String queryString = "select image from astro.images where dr7objid=?";
            image = query.query(conn, queryString, resultSetHandler, id);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(conn);
        }
        return image;
    }

    @Nonnull
    static public GalaxyMetadata getMetadata(String id) {
        GalaxyMetadata metadata = new GalaxyMetadata();
        Connection conn = null;
        if (driver == null) {
            loadProperties();
        }
        DbUtils.loadDriver(driver);
        DriverManager.setLoginTimeout(5);
        try {
            conn = DriverManager.getConnection(url, username, password);
            ResultSetHandler<GalaxyMetadata> resultSetHandler = new ResultSetHandler<GalaxyMetadata>() {
                @Override
                public GalaxyMetadata handle(ResultSet resultSet) throws SQLException {
                    GalaxyMetadata result = new GalaxyMetadata();
                    if (resultSet.next()) {
                        result.setDec(resultSet.getDouble("declination"));
                        result.setRa(resultSet.getDouble("ra"));
                        result.setScale(resultSet.getFloat("petroR90_r"));
                    }
                    return result;
                }
            };
            QueryRunner query = new QueryRunner();
            String queryString = "select ra, declination, petroR90_r from astro.zoo2 natural join astro.petror where dr7objid=?";
            metadata = query.query(conn, queryString, resultSetHandler, id);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(conn);
        }
        return metadata;
    }

    @Nonnull
    public static String getWhereClauseForQuestion(ZooQuestion zooQuestion) {
        return getWhereClauseForQuestion(zooQuestion, 0.0f);
    }

    @Nonnull
    public static String getWhereClauseForQuestion(ZooQuestion zooQuestion, float agreementPercent) {
        switch (zooQuestion) {
            case All:
            case Question01: {
                if (agreementPercent == 0) {
                    // optimize the query slightly
                    return "TRUE";
                } else {
                    StringBuilder builder = new StringBuilder("a01 > ");
                    builder.append(agreementPercent).append(" OR a02 > ").append(agreementPercent);
                    builder.append(" OR a03 > ").append(agreementPercent);
                    return builder.toString();
                }
            }
            case Question02:
                return "a02 > a01 AND a02 > a03";
            case Question03:
            case Question04:
            case Question05:
                return "a02 > a01 AND a02 > a03 AND a05 > a04";
            case Question06:
                return "a01 > a03 AND a02 > a03";
            case Question07:
                return "a01 > a02 AND a01 > a03";
            case Question08:
                return "a01 > a03 AND a02 > a03 AND a14 > a15";
            case Question09:
                return "a02 > a01 AND a02 > a03 AND a04 > a05";
            case Question10:
            case Question11:
                return "a02 > a01 AND a02 > a03 AND a05 > a04 AND a08 > a09";
            default:
                throw new IllegalArgumentException("Unknown task number");
        }
    }

    @Nonnull
    public static List<String> getIdListForQuestion(ZooQuestion zooQuestion, final int limit) {
        return getIdListForQuestion(zooQuestion, limit, 0.0f);
    }

        @Nonnull
    public static List<String> getIdListForQuestion(ZooQuestion zooQuestion, final int limit, final float agreementPercent) {
        Connection conn = null;
        List<String> ids = new ArrayList<String>(limit);

        if (driver == null) {
            loadProperties();
        }
        DbUtils.loadDriver(driver);
        DriverManager.setLoginTimeout(5);
        try {
            conn = DriverManager.getConnection(url, username, password);
            ResultSetHandler<List<String>> resultSetHandler = new ResultSetHandler<List<String>>() {
                @Override
                public List<String> handle(ResultSet resultSet) throws SQLException {
                    List<String> result = new ArrayList<String>(limit);
                    while (resultSet.next()) {
                        result.add(resultSet.getString("dr7objid"));
                    }
                    return result;
                }
            };
            QueryRunner query = new QueryRunner();
            String queryStr = "SELECT dr7objid from astro.category_data WHERE " + getWhereClauseForQuestion(zooQuestion, agreementPercent) + " ORDER BY RAND() " + " LIMIT " + limit;
            ids = query.query(conn, queryStr, resultSetHandler);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(conn);
        }
        return ids;
    }

    @Nullable
    public static String getAnswersForId(final String id) {
        Connection conn = null;
        String answerStr = null;
        if (driver == null) {
            loadProperties();
        }
        DbUtils.loadDriver(driver);
        DriverManager.setLoginTimeout(5);
        try {
            conn = DriverManager.getConnection(url, username, password);
            ResultSetHandler<String> resultSetHandler = new ResultSetHandler<String>() {
                @Override
                public String handle(ResultSet resultSet) throws SQLException {
                    StringBuilder result = new StringBuilder();
                    String[] columnNames = {"a01", "a02", "a03", "a04", "a05", "a06", "a07", "a08", "a09", "a10",
                            "a11", "a12", "a13", "a14", "a15", "a16", "a17", "a18", "a19", "a20",
                            "a21", "a22", "a23", "a24", "a25", "a26", "a27", "a28", "a29", "a30",
                            "a31", "a32", "a33", "a34", "a36", "a37", "a38"};
                    if (resultSet.next()) {
                        for (String column : columnNames) {
                            if (!column.equals("a01")) {
                                result.append(",");
                            }
                            result.append(resultSet.getFloat(column));
                        }
                    }
                    return result.toString();
                }
            };
            QueryRunner query = new QueryRunner();
            String queryStr = "SELECT * FROM astro.category_data WHERE dr7objid = " + id;
            answerStr = query.query(conn, queryStr, resultSetHandler);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(conn);
        }
        return answerStr;
    }

    public static String getPetroForId(final String id){
        Connection conn = null;
        String answerStr = null;
        if (driver == null) {
            loadProperties();
        }
        DbUtils.loadDriver(driver);
        DriverManager.setLoginTimeout(5);

        try {
            conn = DriverManager.getConnection(url, username, password);
            ResultSetHandler<String> resultSetHandler = new ResultSetHandler<String>() {
                @Override
                public String handle(ResultSet resultSet) throws SQLException {
                    StringBuilder result = new StringBuilder();
                    String[] columnNames = {"petroR90_r","petroR50_r","petroR90_u","petroR50_u","petroR90_g",
                            "petroR50_g","petroR90_i","petroR50_i","petroR90_z","petroR50_z"};
                    if (resultSet.next()) {
                        // none of the values are null or 0
                        for (int i = 0; i < columnNames.length; i += 2 ){
                            if (i > 0){
                                result.append(",");
                            }
                            String r90ColumnName = columnNames[i];
                            String r50ColumnName = columnNames[i+1];
                            float ratio = resultSet.getFloat(r90ColumnName) / resultSet.getFloat(r50ColumnName);
                            result.append(ratio);
                        }
                    }
                    return result.toString();
                }
            };
            QueryRunner query = new QueryRunner();
            String queryStr = "SELECT * FROM astro.petror WHERE dr7objid = " + id;
            answerStr = query.query(conn, queryStr, resultSetHandler);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(conn);
        }
        return answerStr;
    }

}
