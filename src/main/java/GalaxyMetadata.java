/**
 * File: GalaxyMetadata.java
 * Project: Image Classifier
 * Author: Kathy Applebaum
 * Created: Aug 30, 2015
 * <p/>
 * Convenience class to bundle database query result
 */
public class GalaxyMetadata {
	private static final float SCALE_FACTOR = 0.014f;
	private static final String DEFAULT_SIZE = "140";

	private String ra;
	private String dec;
	private String scale;
	private String width;
	private String height;

	public String getRa() {
		return ra;
	}

	public void setRa(String ra) {
		this.ra = ra;
	}

	public void setRa(Number ra) {
		this.ra = ra.toString();
	}

	public String getDec() {
		return dec;
	}

	public void setDec(String dec) {
		this.dec = dec;
	}

	public void setDec(Number dec) {
		this.dec = dec.toString();
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public void setScale(Number petroR) {
		Number thisScale = petroR.floatValue() * SCALE_FACTOR;
		this.scale = thisScale.toString();
	}

	public String getWidth() {
		return width == null ? DEFAULT_SIZE : width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public void setWidth(Number width) {
		this.width = width.toString();
	}

	public String getHeight() {
		return height == null ? DEFAULT_SIZE : height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public void setHeight(Number height) {
		this.height = height.toString();
	}
}
